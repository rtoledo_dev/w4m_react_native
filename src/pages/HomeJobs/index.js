/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { Alert, View, AsyncStorage, ActivityIndicator, FlatList } from "react-native";
import api from "services/api";

import JobBox from "components/JobBox";

import Icon from "react-native-vector-icons/FontAwesome";

// var SQLite = require('react-native-sqlite-storage');
// var db = SQLite.openDatabase({
//   name: 'w4m.db',
//   createFromLocation: '~w4mdatabase.db',
// });

import { colors } from "styles";
import styles from "./styles";

export default class HomeJobs extends Component {
  static navigationOptions = {
    title: "Lista de serviços",
    headerTitleStyle: {
      textAlign: "center",
      alignSelf: "center",
      flex: 1,
      color: colors.primary
    },
    tabBarIcon: () => (
      <Icon name="hourglass-half" size={20} color={colors.lighter} />
    )
  };

  state = {
    data: [],
    loading: false
  };

  componentWillMount = () => {
    // this.loadJobs();
    this._loadingJobs()
  };

  _loadingJobs = async () => {
    const auth_token = await AsyncStorage.getItem('@ClientKey:auth_token');
    const response = await api.post('/users/jobs', {
        auth_token: auth_token,
        loading: false,
        errorMessage: null
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    );
    this.setState({ data: response.data.jobs });
  };

  // loadJobs = async () => {
  //   db.transaction((tx) => {
  //     tx.executeSql("select * from jobs order by position desc, job_id desc", (tx, results) => {
  //       console.tron.log(results);
  //     }),
  //   }),
  // };

  renderFlatListItem = item => <JobBox style={styles.height} navigation={this.props.navigation} job={item} />;

  renderList = () => (
    <View>
      <FlatList
        data={this.state.data}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => this.renderFlatListItem(item)}
      />
    </View>
  );

  render() {
    return (
      <View style={styles.container}>
        {this.state.loading ? (
          <ActivityIndicator style={styles.loading} />
        ) : (
          this.renderList()
        )}
      </View>
    );
  }
}
