import axios from 'axios';
const api = axios.create({
  // baseURL: "http://192.168.0.100:8080/w4m/api"
  // baseURL: "http://192.168.0.103:8080/w4m/api"
  // baseURL: "http://192.168.1.106:8080/w4m/api"
  // baseURL: "http://192.168.130.49:8080/w4m/api"
  // baseURL : "http://172.20.10.7:8080/w4m/api"
  // baseURL: "http://192.168.135.242:8080/w4m/api"
  // baseURL: "http://192.168.0.136:8080/w4m/api"
  // baseURL: "http://192.168.0.102:8080/w4m/api"
  // baseURL : "http://10.0.2.49:8080/w4m/api"
  // baseURL: "http://192.168.132.128:8080/w4m/api"
  baseURL: "http://app.pxos.com.br:8080/w4m/api"
  // baseURL: "http://192.168.0.100:8080/w4m/api"
  // baseURL: "http://10.0.2.49:8080/w4m/api"
});

export default api;
