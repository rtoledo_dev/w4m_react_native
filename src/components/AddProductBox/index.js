import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, ScrollView, TextInput, AsyncStorage, Alert, Image } from "react-native";
import { StyleSheet, PixelRatio } from "react-native";
import { colors, metrics } from "styles";
import Modal from 'react-native-modal';
import Button from 'react-native-button';
import ProductBox from './ProductBox';

import styles from './styles';

export default class AddProductBox extends Component {
  state = {
    data: [],
    loading: false
  };

  componentWillMount = () => {
    // this.loadJobs();
    this._loadingProducts()
  };

  _loadingProducts = async () => {
    const auth_token = await AsyncStorage.getItem('@ClientKey:auth_token');
    const response = await api.post('/users/products', {
        auth_token: auth_token,
        loading: false,
        errorMessage: null
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    );
    this.setState({ data: response.data.products });
  };

  renderFlatListItem = item => <ProductBox style={styles.height} navigation={this.props.navigation} job={item} />;

  renderList = () => (
    <View>
      <FlatList
        data={this.state.data}
        keyExtractor={item => String(item.id)}
        renderItem={({ item }) => this.renderFlatListItem(item)}
      />
    </View>
  );
  // constructor(proos){
  //   super(props);
  //   this.state = ({
  //     deletedRowKey: null,
  //   });

  //   refreshKeyList = (deletedKey) => {
  //     this.setState(prevKey) => {
  //       return {
  //         deletedRowKey: deletedKey
  //       };
  //     };
  //   };
};
