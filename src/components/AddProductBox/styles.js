import { StyleSheet, PixelRatio } from "react-native";
import { colors, metrics } from "styles";

const styles = StyleSheet.create({
  flatItemList: {
    color: colors.white,
    padding: metrics.basePadding,
    fontSize: 16,
  },
  
});