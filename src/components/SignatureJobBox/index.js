import React, { Component } from 'react';
import { View, Text, TouchableHighlight, AsyncStorage } from "react-native";
import { withNavigation } from "react-navigation";
import SignatureCapture from 'react-native-signature-capture';
import api from "services/api";

import styles from './styles';

class SignatureJobBox extends Component {
  state = {
    loading: false
  };

  detailJob = async (job) => {
    this.setState({ loading: true });
    try {
      const auth_token = await AsyncStorage.getItem("@ClientKey:auth_token");
      await this.saveJob(job, auth_token); //aqui vai o ajax
      this.setState({ loading: false });
      this.props.navigation.navigate("DetailJob", { job });
    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: ""
      });
    }
  };

  saveJob = async (job, auth_token) => {
    const resultCheckin = await api.post('/jobs/checkin', {
      auth_token: auth_token,
      id: job.id,
      loading: false,
      errorMessage: null
    });
  };

  saveSign() {
      this.refs["sign"].saveImage();
  }

  resetSign() {
      this.refs["sign"].resetImage();
  }

  _onSaveEvent(result) {
  }
  _onDragEvent() {
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column", alignItems:"center",justifyContent:"center" }}>
                <Text style={{alignItems:"center",justifyContent:"center"}}>Captura de firma extendida</Text>
                <SignatureCapture
                    style={[{flex:1},styles.signature]}
                    ref="sign"
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={this._onDragEvent}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>

                <View style={{ flex: 1, flexDirection: "row" }}>
                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.saveSign() } } >
                        <Text>Guardar la firma</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.resetSign() } } >
                        <Text>Borrar</Text>
                    </TouchableHighlight>
                </View>
            </View>
    );
  }
};

export default withNavigation(SignatureJobBox);
