import React, { Component } from 'react';
import {ScrollView, View, Text, TouchableOpacity, ActivityIndicator, AsyncStorage, Alert } from "react-native";
import { withNavigation } from "react-navigation";
import api from 'services/api';

import styles from './styles';

class DetailJobBox extends Component {

  state = {
    loading: false
  };

  componentWillMount = () => {
    let job = this.props.navigation.state.params.job;
    this.setState({ job });
    this.savePosition();
  };

  savePosition = async () => {
    this.setState({ loading: true });
    try {
      const auth_token = await AsyncStorage.getItem('@ClientKey:auth_token');

      navigator.geolocation.watchPosition((position) => {
        const formdata = new FormData();
        formdata.append('auth_token', auth_token);
        formdata.append('latitude',   position.coords.latitude);
        formdata.append('longitude',  position.coords.longitude);

        try {
          api.post('/users/locations/update', formdata);
        } catch (error) {
          Alert.alert('No se pudo sincronizar su ubicación.');
        }
      });
      this.setState({ loading: false });

    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: "Erro na operação"
      });
    }
  };


  finishJob = async () => {
    this.setState({ loading: true });
    try {
      this.setState({ loading: false });
      this.props.navigation.navigate("FinishJob", { job: this.state.job });
    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: "Erro na operação"
      });
    }
  };

  closeJob = async (job) => {
    this.setState({ loading: true });
    try {
      this.setState({ loading: false });
      this.props.navigation.navigate("CloseJob", { job });
    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: "Erro na operação"
      });
    }
  };

  render() {
    return(
      <ScrollView>
        <View style={styles.jobContainer}>
            <View style={styles.row}>
              <View style={styles.rowSpecialAttention}>
                <Text style={styles.title}>OS #{this.state.job.id}</Text>
                <Text style={styles.title}>
                  Situação: {this.state.job.status_name}
                </Text>
                <Text style={styles.subTitle}>Descrição: {this.state.job.description}</Text>
                <Text style={styles.subTitle}>Observação: {this.state.job.emitter_comments}</Text>
              </View>
              <View>
                <View style={styles.addressContainer}>
                  <Text style={[styles.address, styles.subTitle, styles.underline]}>Ponto: {this.state.job.customer.name}</Text>
                  <Text style={[styles.address, styles.subTitle]}>
                    {this.state.job.customer.address_line},{" "}
                    {this.state.job.customer.address_neighborhood} -{" "}
                    {this.state.job.customer.address_city} /{" "}
                    {this.state.job.customer.address_state}
                  </Text>
                </View>
                <TouchableOpacity style={[styles.button, styles.buttonDontRealize]} onPress={() => this.closeJob(this.state.job)}>
                  {this.state.loading ? <ActivityIndicator size="small" color="#fff" /> : <Text
                      style={styles.buttonText}
                    >
                      Não Realizar OS
                    </Text>}
                </TouchableOpacity>

                <TouchableOpacity style={[styles.button, styles.buttonRealize]} onPress={() => this.finishJob(this.state.job)}>
                  {this.state.loading ? <ActivityIndicator size="small" color="#fff" /> : <Text
                      style={styles.buttonText}
                    >
                      Realizar OS
                    </Text>}
                </TouchableOpacity>
              </View>
            </View>
        </View>
      </ScrollView>
    );
  }
};

export default withNavigation(DetailJobBox);
